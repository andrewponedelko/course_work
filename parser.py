dupl_counter2 = 0
dupl_counter3 = 0
with open('2_subflows.txt', 'r') as read_file, open('2_subflows(0).txt', 'w') as write0,\
    open('2_subflows(1).txt', 'w') as write1:
    for i_line in read_file:
        if '(0)' in i_line:
            write0.write(i_line)
        elif '(1)' in i_line:
            write1.write(i_line)
        elif 'Data received is duplicated' in i_line:
            dupl_counter2 += 1
with open('3_subflows.txt', 'r') as read_file, open('3_subflows(0).txt', 'w') as write0,\
    open('3_subflows(1).txt', 'w') as write1, open('3_subflows(2).txt', 'w') as write2:
    for i_line in read_file:
        if '(0)' in i_line:
            write0.write(i_line)
        elif '(1)' in i_line:
            write1.write(i_line)
        elif '(2)' in i_line:
            write2.write(i_line)
        elif 'Data received is duplicated' in i_line:
            dupl_counter3 += 1
counter2 = 0
counter_line2 = 0
num = -10
flag = True
timer2 = 0
with open('2_subflows(0).txt', 'r') as read_file:
    for i_line in read_file:
        if '[node 1]' not in i_line or 'Win=' not in i_line:
            continue
        temp_time = float(i_line.split(' ')[0])
        if flag:
            start_time = temp_time
            flag = False
        part_line = i_line.partition('Ack=')
        if part_line[2] is not None:
            counter_line2 += 1
            old_num = num
            num = int(part_line[2].partition(' Win=')[0])
            if old_num == num:
                counter2 += 1
    timer2 += temp_time - start_time
with open('2_subflows(1).txt', 'r') as read_file:
    flag = True
    for i_line in read_file:
        if 'Win=' not in i_line or '[node 1]' not in i_line:
            continue
        temp_time = float(i_line.split(' ')[0])
        if flag:
            start_time = temp_time
            flag = False
        part_line = i_line.partition('Ack=')
        if part_line[2] is not None:
            counter_line2 += 1
            old_num = num
            num = int(part_line[2].partition(' Win=')[0])
            if old_num == num:
                counter2 += 1
    timer2 += temp_time - start_time
print('процент потери 2 состовляет: ', (counter2 - dupl_counter2) * 100 / counter_line2)
print('средняя задержка 2 состовляет: ', timer2 / counter_line2)
counter3 = 0
counter_line3 = 0
num = -10
timer3 = 0
with open('3_subflows(0).txt', 'r') as read_file:
    flag = True
    for i_line in read_file:
        if '[node 1]' not in i_line or 'Win=' not in i_line:
            continue
        temp_time = float(i_line.split(' ')[0])
        if flag:
            start_time = temp_time
            flag = False
        part_line = i_line.partition('Ack=')
        if part_line[2] is not None:
            counter_line3 += 1
            old_num = num
            num = int(part_line[2].partition(' Win=')[0])
            if old_num == num:
                counter3 += 1
    timer3 += temp_time - start_time
with open('3_subflows(1).txt', 'r') as read_file:
    flag = True
    for i_line in read_file:
        if 'Win=' not in i_line or '[node 1]' not in i_line:
            continue
        temp_time = float(i_line.split(' ')[0])
        if flag:
            start_time = temp_time
            flag = False
        part_line = i_line.partition('Ack=')
        if part_line[2] is not None:
            counter_line3 += 1
            old_num = num
            num = int(part_line[2].partition(' Win=')[0])
            if old_num == num:
                counter3 += 1
    timer3 += temp_time - start_time
with open('3_subflows(2).txt', 'r') as read_file:
    flag = True
    for i_line in read_file:
        if 'Win=' not in i_line or '[node 1]' not in i_line:
            continue
        temp_time = float(i_line.split(' ')[0])
        if flag:
            start_time = temp_time
            flag = False
        part_line = i_line.partition('Ack=')
        if part_line[2] is not None:
            counter_line3 += 1
            old_num = num
            num = int(part_line[2].partition(' Win=')[0])
            if old_num == num:
                counter3 += 1
    timer3 += temp_time - start_time
print('процент потери 3 состовляет:', (counter3 - dupl_counter3) * 100 / counter_line3)
print('средняя задержка 3 состовляет: ', timer3 / counter_line3)
